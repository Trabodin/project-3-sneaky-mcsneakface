﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
    public Image image;
	// Use this for initialization
	void Start ()
    {
        image = GetComponent<Image>();
        image.type = Image.Type.Filled;
        image.fillMethod = Image.FillMethod.Horizontal;
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        float fill = (GameManager.instance.PlayerCurHealth / GameManager.instance.PlayerMaxHealth);
        image.fillAmount = fill;
    }

}
