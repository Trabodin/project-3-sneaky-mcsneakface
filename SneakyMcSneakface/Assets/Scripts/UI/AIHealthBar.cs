﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIHealthBar : MonoBehaviour {

    public Image image;
    public AIPawn owner;
    // Use this for initialization
    void Start () {
        image = GetComponent<Image>();
        image.type = Image.Type.Filled;
        image.fillMethod = Image.FillMethod.Horizontal;
    }
	
	// Update is called once per frame
	void Update () {

        float fill = (owner.AICurHp / GameManager.instance.AIHealth);
        image.fillAmount = fill;
    }
}
