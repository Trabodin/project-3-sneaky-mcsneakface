﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour {

    public Pawn pawn;
    
	public virtual void Start ()
    {
        pawn = GetComponent<Pawn>();
	}
	
}
