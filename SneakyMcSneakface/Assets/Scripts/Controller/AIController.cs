﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : Controller {

    public List<GameObject> Waypoints;
    public enum AIStates
    {
        Wander,
        Hearing,
        chasing
    }
    AIStates CurrentState = AIStates.Wander;

    public int CurrentWayPoint = 0;
    int loop = 0;
    float timer = 0;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {

        switch (CurrentState)
        {
            case AIStates.Wander:
                Wander();
                break;
            case AIStates.Hearing:
                Hearing();
                break;
            case AIStates.chasing:
                Chasing();
                break;
        }

    }


    //Functions ===============
    void Wander()
    {
        Vector2 VectorToWayPoint = pawn.tf.position - Waypoints[CurrentWayPoint].transform.position;
        float angle = Vector2.Angle(VectorToWayPoint, pawn.tf.right);
        //Debug.Log("Angle to Wyapoint :"+ angle);

        if (angle <= 90 || angle > 180)
        {
            pawn.RotateLeft();
        }
        if (angle > 90 && angle < 180)
        {
            pawn.RotateRight();
        }

        pawn.MoveForward();

        //stuff for changing states
        if (CanHear(GameManager.instance.currentPlayer))
        {
            CurrentState = AIStates.Hearing;
        }
        if (CanSee(GameManager.instance.currentPlayer))
        {
            //GameManager.instance.AudioSource.Play();
            CurrentState = AIStates.chasing;
        } 
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (CurrentState == AIStates.Wander)
        {
            if (other.tag == "WayPoint")
            {
                
                if (loop == 0)
                {
                    if (CurrentWayPoint == Waypoints.Count - 1)
                    {
                        loop = 1;
                    }
                    else
                    {
                        CurrentWayPoint = CurrentWayPoint + 1;
                    }
                }
                if (loop == 1)
                {
                    //if he has already made it through the loop once it starts counting down untill he hits the first one
                    if (CurrentWayPoint == 0)
                    {
                        loop = 0;
                        CurrentWayPoint = CurrentWayPoint + 1;
                    }
                    else
                    {
                        CurrentWayPoint = CurrentWayPoint - 1;
                    }
                }
            }
        }     
    }   

    void Hearing()
    {
        //want him to stop and check in the direction he heard the person
        Vector2 VectorToPlayer = pawn.tf.position - GameManager.instance.currentPlayer.transform.position;
        float angle = Vector2.Angle(VectorToPlayer, pawn.tf.right);
        
        if (angle < 180 && angle > 90)
        {
            pawn.RotateRight();
        }
        if (angle <90)
        {
            pawn.RotateLeft();
        }

        //stuff for changing states
        if (!CanHear(GameManager.instance.currentPlayer) && !CanSee(GameManager.instance.currentPlayer))
        {
            CurrentState = AIStates.Wander;
        }

        if (CanSee(GameManager.instance.currentPlayer))
        {
            CurrentState = AIStates.chasing;
        }
    }

    void Chasing()
    {
        //timer used for the rof
        timer = timer + Time.deltaTime;

        Vector2 VectorToPlayer = pawn.tf.position - GameManager.instance.currentPlayer.transform.position;
        float angle = Vector2.Angle(VectorToPlayer, pawn.tf.right);

        if (angle < 90 || angle > 180)
        {
            pawn.RotateLeft();
        }
        if (angle > 90 && angle < 180)
        {
            pawn.RotateRight();
        }

        pawn.MoveForward();

        //waits till the timer is equal to the time you want to wait before shoting
        if(timer >= GameManager.instance.ROF)
        {
            pawn.Shoot();
            timer = 0;
        }

        //stuff for changing states
        //if he cant see but can here he goes to hearing but if he just cant see he goes back to wander
        if (!CanSee(GameManager.instance.currentPlayer) && CanHear(GameManager.instance.currentPlayer))
        {
            CurrentState = AIStates.Hearing;
        }
        else if (!CanSee(GameManager.instance.currentPlayer))
        {
            CurrentState = AIStates.Wander;
        }

    }

    //checks if the ai can see the player
    public bool CanSee(GameObject player)
    {
        Collider2D target = player.GetComponent<Collider2D>();
        if(target == null)
        {
            return false;
        }

        Vector2 VectorToTarget = GameManager.instance.currentPlayer.transform.position - pawn.transform.position;
       // float angle = Vector2.Angle(VectorToTarget, pawn.transform.right);

        if ((Vector3.Angle(VectorToTarget, pawn.transform.up)) >= (GameManager.instance.FOV))
        {
            return false;
        }

        RaycastHit2D hitinfo = Physics2D.Raycast(pawn.transform.position, VectorToTarget, GameManager.instance.VeiwDistance);
        
        //if the collider is the one were looking for return true
        if (hitinfo.collider == target)
        {
            return true;
        }
        return false;
    }

    public bool CanHear(GameObject player)
    {
        //gets the vector to the player then gets its distance and how far the hearing and noise equate to
        Vector2 VectorToTarget = GameManager.instance.currentPlayer.transform.position - pawn.transform.position;
        float HearingDistance = GameManager.instance.playerNoise + GameManager.instance.AIHearingDistance;
        float Seperation = VectorToTarget.magnitude;

        //if the hearing and noise values add up then he can hear you
        if (Seperation <= HearingDistance && GameManager.instance.PlayerIsMoving== true)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
}
