﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller{

	// Use this for initialization
	public override void Start ()
    {
        base.Start();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            pawn.MoveForward();
            //tells wether the player is moving or not so it can be used to the AISEnse Hearing
            GameManager.instance.PlayerIsMoving = true;
        }
        else if(!(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)))
        {
            GameManager.instance.PlayerIsMoving = false;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            pawn.MoveBackward();
            GameManager.instance.PlayerIsMoving = true;
        }
        else if (!(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)))
        {
            GameManager.instance.PlayerIsMoving = false;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            pawn.RotateLeft();
            
        }
       
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            pawn.RotateRight();
           
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            pawn.Shoot();
        }
                                  
    }
   void OnTriggerEnter2D(Collider2D other)
   {
       if(other.tag == "WinZone")
        {
            GameManager.instance.DoWin();
        }
        if (other.tag == "Bullet")
        {
            GameManager.instance.PlayerCurHealth -= GameManager.instance.AIDamage;
        }
    }
    void OnCollisionEnter2D(Collision2D Other)
    {
      

    }
}
