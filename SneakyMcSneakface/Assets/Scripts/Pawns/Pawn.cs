﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {
    public Transform tf;

	// Use this for initialization
	public virtual void Start ()
    {
        tf = GetComponent<Transform>();
	}

    public virtual void Update()
    {

    }

    public virtual void MoveForward()
    {

    }

    public virtual void MoveBackward()
    {

    }

    public virtual void RotateLeft()
    {

    }

    public virtual void RotateRight()
    {

    }
    public virtual void Shoot()
    {

    }


}
