﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanPawn : Pawn {

	// Use this for initialization
	public override void Start ()
    {
        base.Start();
	}
	
	// Update is called once per frame
	public override void Update ()
    {
        base.Update();
       
	}
    public override void MoveForward()
    {
        tf.transform.Translate(0, Time.deltaTime * GameManager.instance.playerMoveSpeed,  0);
    }

    public override void MoveBackward()
    {
        tf.transform.Translate(0, -Time.deltaTime * GameManager.instance.playerMoveSpeed, 0);
    }

    public override void RotateLeft()
    {
        tf.transform.Rotate(0, 0, Time.deltaTime * GameManager.instance.playerRotationSpeed);
    }

    public override void RotateRight()
    {
        tf.transform.Rotate(0, 0, -Time.deltaTime * GameManager.instance.playerRotationSpeed);
    }
    public override void Shoot()
    {
        //a vector so that the bullet dosent hit the person it shoots out of
        Vector3 distance =  tf.position +(tf.rotation * new Vector3(0, .5f, 0));
        Instantiate(GameManager.instance.Bullet,distance, tf.rotation);
    }


}
