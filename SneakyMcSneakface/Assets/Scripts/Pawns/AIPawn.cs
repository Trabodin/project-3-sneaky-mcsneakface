﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPawn : Pawn {

    public GameObject HealthBar;
    public float AICurHp = 50;
   
    // Use this for initialization
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        HealthBar.transform.position = tf.position + new Vector3(0, .4f, 0);
        if (AICurHp <= 0)
        {
            Destroy(HealthBar);
            Destroy(gameObject);
        }
    }
    public override void MoveForward()
    {
        tf.transform.Translate(0, Time.deltaTime * GameManager.instance.AIMoveSpeed, 0);
    }

    public override void MoveBackward()
    {
        tf.transform.Translate(0, -Time.deltaTime * GameManager.instance.AIMoveSpeed, 0);
    }

    public override void RotateLeft()
    {
        tf.transform.Rotate(0, 0, Time.deltaTime * GameManager.instance.AIRotationSpeed);
    }

    public override void RotateRight()
    {
        tf.transform.Rotate(0, 0, -Time.deltaTime * GameManager.instance.AIRotationSpeed);
    }
    public override void Shoot()
    {
        Vector3 distance = tf.position + (tf.rotation * new Vector3(0, .5f, 0));
        Instantiate(GameManager.instance.Bullet, distance, tf.rotation);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
           AICurHp -= GameManager.instance.PlayerDamage;
        }
    }
}
