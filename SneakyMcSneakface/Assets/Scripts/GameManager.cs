﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    //stuff for the player ===============
    public List<GameObject> players;
    public GameObject currentPlayer;

    public float playerMoveSpeed;
    public float playerRotationSpeed;
    public float playerNoise;
    public bool PlayerIsMoving;

    public float PlayerMaxHealth;
    public float PlayerCurHealth;
    public float PlayerDamage;

    public GameObject homeSpot;


    //AI stuff ===============
    public List<GameObject> Enemies;
    public List<GameObject> EnemyStartPoints;
    public List<GameObject> WayPoints;
    public GameObject HealthBar;
    public GameObject EnemyGuard;

    public float FOV;
    public float VeiwDistance;
    public float AIMoveSpeed;
    public float AIRotationSpeed;
    public float AIHearingDistance;

    public float AIHealth;
    public float AIDamage;
    public float ROF;


    //Other Stuff ===============
    public GameObject Bullet;


    //stuff Ui ===============
    public GameObject GameFeild;
    public Canvas HealthBarCanvas;
    [Range (1, 100)]
    public float MaxTime;
    [HideInInspector]
    public float timeLeft;
    public List<GameObject> menues;


    //sounds? ===============
    public AudioSource AudioSource;

    // called before anything else
    void Awake()
    {
        // makes sure theres only one
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        players[0].SetActive(true);
       //players[1].SetActive(false);
        currentPlayer = players[0];

        timeLeft = MaxTime;
    }

    // Update is called once per frame
    void Update () 
	{
        if (menues[0].activeInHierarchy == true)
        {
            if (timeLeft> 0)
            {
                timeLeft -= Time.deltaTime;
            }
        }
        
        if (timeLeft <= 0)
        {
            DoGameOver();
        }
        if (PlayerCurHealth <= 0)
        {
            DoGameOver();
        }
    }

    //Functions ===============
    public void DoGameOver()
    {
        Reset();
        menues[0].SetActive(false);
        menues[2].SetActive(true);
       
    }

    public void DoWin()
    {
        Reset();
        menues[0].SetActive(false);
        menues[3].SetActive(true);
    }

    private void Reset()
    {
        if (Enemies[0] == null)
        {
            //resets literally everything
            GameObject NewGuard = Instantiate(EnemyGuard, EnemyStartPoints[0].transform.position, EnemyStartPoints[0].transform.rotation);
            GameObject NewHealthBar = Instantiate(HealthBar, EnemyStartPoints[0].transform.position, EnemyStartPoints[0].transform.rotation);
            Enemies.Add(NewGuard);
            AIController C = NewGuard.GetComponent<AIController>();
            AIPawn P = NewGuard.GetComponent<AIPawn>();
            AIHealthBar H = NewHealthBar.GetComponentInChildren<AIHealthBar>();
            NewGuard.transform.SetParent(GameFeild.transform);
            NewHealthBar.transform.SetParent(HealthBarCanvas.transform);
            P.HealthBar = NewHealthBar;
            H.owner = P;
            C.Waypoints = WayPoints;
        

        }
        else
        {
            //if the enemy isnt dead it sets him back to the start
            Enemies[0].transform.position = EnemyStartPoints[0].transform.position;
            Enemies[0].transform.rotation = EnemyStartPoints[0].transform.rotation;
        }
    }

    //Buttons ===============
    public void QuitButton()
    {
        Application.Quit();
    }


    public void ReturnButton()
    {
        currentPlayer.transform.position = homeSpot.transform.position;
        currentPlayer.transform.rotation = homeSpot.transform.rotation;
        timeLeft = MaxTime;
        PlayerCurHealth = PlayerMaxHealth;
        menues[2].SetActive(false);
        menues[3].SetActive(false);
        menues[1].SetActive(true);
    }
    public void StartButton() 
    {
        
        menues[1].SetActive(false);
        menues[0].SetActive(true);
       
    }
}

