﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    Vector2 Direction;
    Transform tf;
    public float BulletSpeed;

	// Use this for initialization
	void Start ()
    {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        tf.transform.Translate(0, Time.deltaTime * BulletSpeed, 0);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "WayPoint")
        {
            Destroy(gameObject);
        }
    }
    
}
